'use strict';

App.factory('ClazzService', ['$http', '$q', function($http, $q){

    return {
        fetchAllClazzA: function() {
            return $http.get('http://localhost:8080/GenericTesting/clazza/')
                .then(
                function(response){
                    return response.data;
                },
                function(errResponse){
                    console.error('Error while fetching clazzes');
                    return $q.reject(errResponse);
                }
            );
        },

        fetchAllClazzB: function() {
            return $http.get('http://localhost:8080/GenericTesting/clazzb/')
                .then(
                function(response){
                    return response.data;
                },
                function(errResponse){
                    console.error('Error while fetching clazzes');
                    return $q.reject(errResponse);
                }
            );
        }
    };
}]);
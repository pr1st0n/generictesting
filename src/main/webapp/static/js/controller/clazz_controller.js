'use strict';

App.controller('ClazzController', ['$scope', 'ClazzService', function($scope, ClazzService) {
    var self = this;
    self.clazz={id:null,name:''};
    self.clazzesA=[];
    self.clazzesB=[];
    $scope.clazzA=[];
    $scope.clazzB=[];

    self.fetchAllClazzes = function(){
        ClazzService.fetchAllClazzA()
            .then(
            function(d) {
                self.clazzesA = d;
                $scope.clazzA = $scope.clazzA.concat(self.clazzesA);
            },
            function(errResponse){
                console.error('Error while fetching Currencies');
            }
        );
        ClazzService.fetchAllClazzB()
            .then(
            function(d) {
                self.clazzesB = d;
                $scope.clazzB = $scope.clazzB.concat(self.clazzesB);
            },
            function(errResponse){
                console.error('Error while fetching Currencies');
            }
        );
    };

    self.fetchAllClazzes();
}]);
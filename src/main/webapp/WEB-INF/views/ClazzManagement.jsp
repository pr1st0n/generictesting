<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<!doctype html>
<html ng-app="app" class="ng-cloak">
    <head>
        <script src="<c:url value='/lib/angular/angular.min.js' />"></script>
        <script src="<c:url value='/lib/angular-animate/angular-animate.js' />"></script>
        <script src="<c:url value='/lib/angular-bootstrap/ui-bootstrap-tpls.js' />"></script>
        <script src="<c:url value='/lib/angular-ui-grid/ui-grid.min.js' />"></script>
        <link href="<c:url value='/static/css/app.css' />" rel="stylesheet">
        <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<c:url value='/lib/angular-ui-grid/ui-grid.min.css' />">
        <script src="<c:url value='/static/js/app.js' />"></script>
        <script src="<c:url value='/static/js/controller/clazz_controller.js' />"></script>
        <script src="<c:url value='/static/js/service/clazz_service.js' />"></script>
    </head>
    <body>
    <header>
        <h1 align="center">Generic Testing</h1>
    </header>
    <div ng-controller="ClazzController">
        <div class="main-container">
            <uib-tabset>
                <uib-tab heading="ClazzA">
                    <div ui-grid="{ data: clazzA }" class="grid" ui-grid-infinite-scroll></div>
                </uib-tab>
                <uib-tab heading="ClazzB">
                    <div ui-grid="{ data: clazzB }" class="grid" ui-grid-infinite-scroll></div>
                </uib-tab>
            </uib-tabset>
        </div>
    </div>
    </body>
</html>
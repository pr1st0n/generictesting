package com.generictesting.service;

import com.generictesting.dao.ClazzDao;
import com.generictesting.dao.GenericDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.generictesting.model.ClazzB;

@Service("ClazzBService")
@Transactional
public class ClazzBServiceImpl extends GenericServiceImpl<ClazzB, Long> implements ClazzService<ClazzB> {

    @Autowired
    private ClazzDao<ClazzB> clazzBDao;

    @Autowired
    public ClazzBServiceImpl(
            @Qualifier("clazzBDaoImpl") GenericDao<ClazzB, Long> genericDao) {
        super(genericDao);
        this.clazzBDao = (ClazzDao<ClazzB>) genericDao;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public ClazzB getClazz(String name) {
        return clazzBDao.getClazz(name);
    }
}

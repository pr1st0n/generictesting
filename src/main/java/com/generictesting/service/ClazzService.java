package com.generictesting.service;

import com.generictesting.model.ClazzA;

public interface ClazzService<Clazz> extends GenericService<Clazz, Long> {
    Clazz getClazz(String name);
}

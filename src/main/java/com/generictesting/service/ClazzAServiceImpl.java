package com.generictesting.service;

import com.generictesting.dao.ClazzDao;
import com.generictesting.dao.GenericDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.generictesting.model.ClazzA;

@Service("clazzAService")
@Transactional
public class ClazzAServiceImpl extends GenericServiceImpl<ClazzA, Long> implements ClazzService<ClazzA> {

    @Autowired
    private ClazzDao<ClazzA> clazzADao;

    @Autowired
    public ClazzAServiceImpl(
            @Qualifier("clazzADaoImpl") GenericDao<ClazzA, Long> genericDao) {
        super(genericDao);
        this.clazzADao = (ClazzDao<ClazzA>) genericDao;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public ClazzA getClazz(String name) {
        return clazzADao.getClazz(name);
    }
}

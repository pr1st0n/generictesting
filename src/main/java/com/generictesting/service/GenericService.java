package com.generictesting.service;

import java.util.List;

public interface GenericService<E, K> {
    public List<E> findAllItems();
    public E get(K id);
}

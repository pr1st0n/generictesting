package com.generictesting.dao;

public interface ClazzDao<Clazz> extends GenericDao<Clazz, Long> {
    Clazz getClazz(String name);
}

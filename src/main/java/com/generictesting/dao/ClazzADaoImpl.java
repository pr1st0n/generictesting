package com.generictesting.dao;

import com.generictesting.model.ClazzA;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

@Repository
public class ClazzADaoImpl extends GenericDaoImpl<ClazzA, Long> implements ClazzDao<ClazzA> {

    @Override
    public ClazzA getClazz(String name) {
        Query query = currentSession().createQuery("from ClazzA where name=:name");
        query.setParameter("name", name);
        return (ClazzA) query.uniqueResult();
    }
}

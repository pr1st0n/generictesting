package com.generictesting.dao;

import com.generictesting.model.ClazzB;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

@Repository
public class ClazzBDaoImpl extends GenericDaoImpl<ClazzB, Long> implements ClazzDao<ClazzB> {

    @Override
    public ClazzB getClazz(String name) {
        Query query = currentSession().createQuery("from ClazzB where name=:name");
        query.setParameter("name", name);
        return (ClazzB) query.uniqueResult();
    }
}

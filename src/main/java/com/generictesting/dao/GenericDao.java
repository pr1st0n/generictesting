package com.generictesting.dao;

import java.util.List;

public interface GenericDao<E, K> {
    public E find(K key);
    List<E> findAllItems();
}

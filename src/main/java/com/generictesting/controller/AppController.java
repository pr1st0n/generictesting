package com.generictesting.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.generictesting.model.ClazzA;
import com.generictesting.service.ClazzService;
import com.generictesting.model.ClazzB;

import java.util.List;

@RestController
public class AppController {

    @Autowired(required = true)
    ClazzService<ClazzA> clazzAService;

    @Autowired(required = true)
    ClazzService<ClazzB> clazzBService;

    @RequestMapping(value = "/clazza", method = RequestMethod.GET)
    public ResponseEntity<List<ClazzA>> listClazzA() {
        List<ClazzA> items = clazzAService.findAllItems();
        if(items.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<>(items, HttpStatus.OK);
    }

    @RequestMapping(value = "/clazzb", method = RequestMethod.GET)
    public ResponseEntity<List<ClazzB>> listClazzB() {
        List<ClazzB> items = clazzBService.findAllItems();
        if(items.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<>(items, HttpStatus.OK);
    }

    @RequestMapping(value = "/clazza/{name}", method = RequestMethod.GET)
    public ResponseEntity<ClazzA> getClazzA(@PathVariable String name) {
        ClazzA item = clazzAService.getClazz(name);
        if(item == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<>(item, HttpStatus.OK);
    }

    @RequestMapping(value = "/clazzb/{name}", method = RequestMethod.GET)
    public ResponseEntity<ClazzB> getClazzB(@PathVariable String name) {
        ClazzB item = clazzBService.getClazz(name);
        if(item == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<>(item, HttpStatus.OK);
    }
}
